"""Module to test the log parser."""

from collections.abc import Iterator
from pathlib import Path

import pytest

from sc_parser.log_data import LogData


def get_all_test_data() -> Iterator[tuple[Path, str]]:
    """Yield tuples containing test file names and their expected results."""
    data_dir = Path(__file__).parent / "data"
    for data_path in (data_dir / "input").glob("*.log"):
        expected_path = data_dir / f"expected/{data_path.name}"

        with expected_path.open(encoding="utf-8") as expected_file:
            yield data_path, expected_file.read()


@pytest.mark.parametrize("log_path,expected", get_all_test_data())
def test_parse_file(log_path: Path, expected: str) -> None:
    assert LogData(log_path).parse() == expected
