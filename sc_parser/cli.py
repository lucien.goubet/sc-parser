"""Command line entrypoint."""

from pathlib import Path

import typer
from typing_extensions import Annotated

from sc_parser.log_data import LogData

cli = typer.Typer()


@cli.command()
def parse(path: Annotated[Path, typer.Argument(help="Log data to parse.")]) -> None:
    """Parse log file and print result."""
    print(LogData(path).parse())  # noqa: T201


def main() -> None:
    """CLI entrypoint."""
    cli()
