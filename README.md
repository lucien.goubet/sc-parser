### sc-parser

**Description:**

sc-parser is a Python tool designed to parse a specified log file according to a set of rules and display formatted output to the screen.

### Installation

Download dependencies and install the script, followed by entering the Python environment.

```
poetry install
poetry shell
```


### Usage:

```
sc-parser data.log
```

### Output:

For each line in the input file, the tool will display a line in the format: "LINE_NUMBER : NEW_DATA"

### Calculation of LINE_NUMBER:

The line number corresponds to the line number in the input file, starting from 0 for the first line.

### Calculation of NEW_DATA:

- If LINE_NUMBER is a multiple of 5, NEW_DATA is set to "Multiple de 5" (where 0 is considered a multiple of any integer).
- If the input line contains at least one '$' character, all space characters are replaced by '_' and the result is assigned to NEW_DATA.
- If the line ends with a '.', disregarding end-of-line characters '\r' and '\n', NEW_DATA is set to the input line.
- If the first character of the input line is '{', the line is deserialized as JSON format. Then, a 'pair' key is added to the dictionary with a value of True if LINE_NUMBER is even (False otherwise). Finally, the modified dictionary is serialized back to JSON format and assigned to NEW_DATA.
- If none of the previous conditions are met, NEW_DATA is set to 'Rien à afficher' (Nothing to display).

If multiple rules apply for the calculation of NEW_DATA, only the rule listed first will be applied.

### Example:

Input:
```
This is a test.
$
{"key": "value"}
Another line.
This is the fifth line.
Start of a new line.
hello world
```

Output:
```
0 : Multiple de 5
1 : _
2 : {"key": "value", "pair": true}
3 : Another line.
4 : This is the fifth line.
5 : Multiple de 5
6 : Rien à afficher
```
