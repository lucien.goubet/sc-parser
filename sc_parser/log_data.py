"""The Log data module. It contains all the necessaries methods to parse the log."""

import json
import re
from pathlib import Path


class LogData:
    def __init__(self, log_path: Path) -> None:  # noqa: D107
        self.log_path = log_path

    def parse(self) -> str:
        """Parse file, Ensure that the rules are followed in their specified order of priority."""
        with self.log_path.open(encoding="utf-8") as log_file:
            all_lines = log_file.read().splitlines()

        return "\n".join([f"{num} : {self.parse_line(num, line)}" for num, line in enumerate(all_lines)])

    def parse_line(self, num: int, line: str) -> str:
        """
        Parse file, Ensure that the rules are followed in their specified order of priority.
        If none of the previous conditions are met, NEW_DATA is set to 'Rien à afficher' (Nothing to display).
        """
        return (
            self._is_multiple_of_five(num)
            or self._contains_dollar(line)
            or self._ends_with_dot(line)
            or self._is_json_data(num, line)
            or "Rien à afficher"
        )

    @staticmethod
    def _is_multiple_of_five(num: int) -> str | None:
        """
        If LINE_NUMBER is a multiple of 5, NEW_DATA is set to "Multiple de 5"
        (where 0 is considered a multiple of any integer). Return None otherwise.
        """
        return "Multiple de 5" if num % 5 == 0 else None

    @staticmethod
    def _contains_dollar(line: str) -> str | None:
        """
        If the input line contains at least one '$' character, all space characters are replaced by '_'
        and the result is assigned to NEW_DATA. Return None otherwise.
        """
        return line.replace("$", "_") if "$" in line else None

    @staticmethod
    def _ends_with_dot(line: str) -> str | None:
        r"""
        If the line ends with a '.', disregarding end-of-line characters '\r' and '\n',
        NEW_DATA is set to the input line. Return None otherwise.
        """
        return line if re.match(r"^.*\.$", line) else None

    @staticmethod
    def _is_json_data(num: int, line: str) -> str | None:
        """
        If the first character of the input line is '{', the line is deserialized as JSON format.
        Then, a 'pair' key is added to the dictionary with a value of True if LINE_NUMBER is even (False otherwise).
        Finally, the modified dictionary is serialized back to JSON format and assigned to NEW_DATA.
        Return None otherwise. If the string cannot be parsed by the json library, the method returns None.
        """
        if line == "" or line[0] != "{":
            return None

        try:
            return json.dumps(json.loads(line) | {"pair": num % 2 == 0}, ensure_ascii=False)
        except json.decoder.JSONDecodeError:
            return None
