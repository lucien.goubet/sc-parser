# Improvements
Hey there! Below some ideas on how we can take this project to the next level and get it ready for production! Let's check them out.

* CI/CD: create a CI/CD pipeline (gitlab ci/cd, github action, jenkins, ...) to automatic push new feature in production:
  - **static tests** : re-execute the [pre-commit](https://pre-commit.com/) and add sonarcube
  - **dynamic tests**: the unittests I wrote should be also executed in the ci/cd
  - **semantic-versioning**: use semantic versioning and conventional commits to automatically update version and changelogs: I usually use this [tool](https://python-semantic-release.readthedocs.io/en/latest/index.html).
  - **packaging**: for now I just showed in the readme how to execute the code in the poetry environment. But this is just for testing. In production we should deliver a docker image/debian package/wheel package/...
  - **deployment**: push to pip repository / docker hub / AWS / GCP

* Add more unittests
* Add a changelog
